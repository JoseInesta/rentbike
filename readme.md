# E-bike finder app

This repository contains the source code of an awesome E-bike finder app made with Qt and Felgo.

![Application preview](./tutorial/assets/img/main.png)

You can find a tutorial on how to make it from scratch [here](https://gitlab.com/JoseInesta/rentbike/-/blob/main/tutorial/tutorial.md).
