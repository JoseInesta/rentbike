# Create an awesome E-bike finder app with Qt and Felgo from scratch.

![Application preview](./assets/img/main.png)

## Index

1. [Introduction](#id1)
2. [Felgo and Qt](#id2)
3. [Getting started](#id3)
4. [Folder and files structure](#id4)
5. [Main.qml](#id5)
6. [Components](#id9)
7. [Pages](#id6)
8. [Model](#id7)
9. [Logic](#id8)
10. [App publication](#id10)

## Introduction <a name="id1"></a>

In this tutorial we explain how to create a stylish E-bike finder application from scratch. You will learn how to structure your application, generate the different pages and organise the navigation between the different elements.

![Application preview](./assets/mockups/mockup05.png)

The application will allow the user the following basic functions:

1. View available bikes.
2. Search for bikes filtering by its properties (name, price...)
3. Locate available bikes on a map.
4. Know the details and characteristics of the different models.

> The complete source code for this tutorial is available [here](https://gitlab.com/JoseInesta/rentbike). Feel free to comment and propose new improvements that you find interesting.

![Application preview](./assets/mockups/mockup06.jpg)

## Felgo and Qt <a name="id2"></a>

The application will be developed with Felgo and Qt/QML. Felgo is a cross-platform development SDK based on the popular Qt framework used by more than 800,000 developers. The generated code will allow you to compile the application for desktop, Android and IOS environments.

To do this tutorial you need to have Qt and Felgo installed. You can find instructions on how to install Felgo [here](https://felgo.com/doc/felgo-installation/). You can also find very interesting tutorials and documentation on creating applications with Felgo [here](https://felgo.com/doc/apps-getting-started/).

> In this tutorial we have used Windows 10, but you can use other platforms such as MAC or Linux.

## Getting started <a name="id3"></a>

The first step is to open Qt Creator and create a blank project on which to build the different sections of our application.

### Open Qt Creator

#### Windows

You can open Qt Creator from your start menu, or use the Explorer to navigate to your installation Folder.

#### Mac

You can use Spotlight Search (CMD + Spacebar by default) and type "Qt Creator" or use the Finder to navigate to your installation Folder.

#### Linux

Depending on your Linux distribution, you will find a *Qt Creator* shortcut in your programs search or you will need to navigate to your Qt Creator folder and execute it via console.

### Create a New Project

To create a new project, select the *Welcome* tab. Then click on the *Create Project* button. Alternatively you could select *File* > *New File or Project...* in the menu bar.

![Application preview](./assets/img/00.png)

Now we'll have a quick look at the steps of the wizard that has just opened.

#### Templates

First of all we can choose which type of file or project we want to create. For Felgo projects, there are a number of templates, which are mostly complete demo games or apps.

For this tutorial first select *Felgo Apps* at Projects tab, then *Single-Page Applicationn* and *Choose* button.

![Application preview](./assets/img/01.png)

#### Location

Next we specify the project name and the location. Press *Next* when you are done.

![Application preview](./assets/img/02.png)

#### Kits

Qt Creator groups settings used for building and running projects as so-called kits in order to make deployment to different platforms easier. These kits define environments like the target platform, compilers, Qt versions and devices. The initial Felgo installation only includes the kit for your current Desktop system to make the download as small as possible.

Make sure your desktop kit is selected and press *Next* to proceed.

![Application preview](./assets/img/03.PNG)

#### Felgo Projects

Now we add some additional information about our game.

**App display name**: Enter a name that should be displayed on the home-screen of your mobile game or as the title of your desktop game.

**App identifier**: A unique name used for the bundle identifier on iOS and as package name on Android. It may contain uppercase or lowercase letters ('A' through 'Z'), numbers and periods ("."), however individual identifier name parts may only start with letters. To avoid conflicts with other developers, you should use Internet domain ownership as the basis for your package names (in reverse; e.g. com.felgo.example.game).

**Interface orientation**: Choose whether your game should be in landscape mode, portrait mode or should automatically rotate depending on the device orientation.

For this tutorial, we use the *Portrait* orientation. Press *Next* when you are done.

![Application preview](./assets/img/04.PNG)

#### Plugins

In this step we can include different Felgo plugins. In our tutorial we will not need any of them. Press *Next* when you are done.

![Application preview](./assets/img/05.PNG)

#### Integration

Since we did not select any plugins in the previous step, we do not need to do anything in this one.  Press *Next* when you are done.

![Application preview](./assets/img/06.PNG)

#### Summary

Finally, in this last step we can configure the version of controls and other parameters. In our example, it is not necessary to make any settings. Press *Next* when you are done.

![Application preview](./assets/img/07.PNG)

If everything went well, you will see the main Qt Creator window with the structure of the newly created project:

![Application preview](./assets/img/08.PNG)

You will also see the *Felgo Live Client* and the *Felgo Live Server* console:

![Application preview](./assets/img/11.PNG)

You are now ready to start creating your bike rental app.

## Folder and files structure <a name="id4"></a>

For our application, we will mainly work with the QML files contained in the sub-folder /qml.

![Application preview](./assets/img/files.PNG)

The folders and files generated are:

- /qml/components/*: includes different files with reusable elements.
- /qml/logic/*: files related to the behaviour of the application.
- /qml/model/*: data and model information of the bikes and the app.
- /qml/pages/*: files corresponding to the different sections of the app.
- /qml/main.qml: root application file

## Main.qml <a name="id5"></a>

Main.qml file is the root file of the application where the parameters are initialised and different actions are performed, such as the configuration of the app's styles, the configuration of the navigation or the creation of the different instances of the objects.

### Theme Initialisation

First we will initialise the global styles of the application and custom fonts:

    // initialize theme styles
    onInitTheme: {
        //fonts
        Theme.normalFont = poppinsFontNormal
        Theme.boldFont = poppinsFontBold

        //buttons
        Theme.appButton.backgroundColor = 'black'
        Theme.appButton.radius = sp(30)
        Theme.appButton.flatTextColor = 'white'
        Theme.appButton.dropShadow = false
        Theme.appButton.textSize = sp(4)
        Theme.appButton.textColor = 'white'
        Theme.appButton.textColorDisabled = 'white'

        //colors
        Theme.colors.textColor = 'black'
        Theme.colors.secondaryTextColor = 'grey'
        Theme.colors.secondaryBackgroundColor = 'black'
        Theme.colors.tintColor = 'grey'

        //navigation bar
        //Theme.navigationBar.backgroundImageSource= "../assets/img/background.jpg"
        //Theme.navigationBar.backgroundImageFillMode= Image.PreserveAspectCrop
        Theme.navigationBar.backgroundColor= 'black'
        Theme.navigationBar.shadowHeight= 0
        Theme.navigationBar.itemColor= 'white'

        //dialogs
        Theme.dialog.titleTextSize = 12
        Theme.dialog.buttonTextSize = 12
    }

    // load new fonts with FontLoader
    FontLoader {
        id: poppinsFontNormal
        source: "../assets/fonts/Poppins-Regular.ttf" // loaded from your assets folder
    }

    FontLoader {
        id: poppinsFontBold
        source: "../assets/fonts/Poppins-SemiBold.ttf" // loaded from your assets folder
    }

### Instantiation

We generate the instances and objects of the business logic and data models. We choose unique identifiers that will be accessible in all other sections of the code.

    // business logic
    Logic {
        id: logic
    }

    // data model for storing bikes
    DataModel {
        id: dataModel
    }

    // app model for store user connection stares
    AppModel {
        id: appModel
        dispatcher: logic //handles actions sent by logic
    }

### Navigation

In this portion of the code the navigation stack is configured. Depending on the platform we will see it as a side menu (Android and Desktop) or tabs navigation (IOS).

    // view
    Navigation {
        id: navigation

        // only enable if user has entered in
        // intro page below overlays navigation then
        enabled: appModel.userEntered

        // Image at top
        headerView: AppImage {
          width: parent.width
          height: 100
          source: "../assets/img/bikeBackground.png"
          fillMode: AppImage.PreserveAspectCrop
        }

        // Home tab
        NavigationItem {
            title: qsTr("Home")
            icon: IconType.home
            NavigationStack {
                id: homeNavigationStack
                splitView: tablet // use side-by-side view on tablets
                initialPage: HomePage {
                    onSearchRequested: homeNavigationStack.push(searchPageComponent, {"searchTerm": term})
                }
            }
        }

        // Search tab
        NavigationItem {
            title: qsTr("Search")
            icon: IconType.search
            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: SearchPage { }
            }
        }

        // Map tab
        NavigationItem {
            title: qsTr("Map")
            icon: IconType.mapmarker
            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: MapPage { }
            }
        }
    }

    Component {
        id: detailPageComponent
        DetailPage { }
    }

    Component {
      id: searchPageComponent
      SearchPage { }
    }

### Intro page

This code allows the *Intro* page to be displayed until the user clicks on the enter button. For a smooth transition, an animation has been configured on the opacity.

    // Intro page lies on top of previous items and overlays if user is not entered in
    IntroPage {
        visible: opacity > 0
        enabled: visible
        opacity: appModel.userEntered ? 0 : 1 // hide if user is entered in
        Behavior on opacity { NumberAnimation { duration: 250 } } // page fade in/out
    }


## Components <a name="id9"></a>

QML has the ability to define your own QML components that suits the purpose of your application. The standard QML elements provide the essential components for creating a QML application; beyond these, you can write your own custom components that can be created and reused.

Components are the building blocks of a QML project. When writing a QML application, whether large or small, it is best to separate QML code into smaller components that perform specific sets of operations, instead of creating mammoth QML files with large, combined functionality that is more difficult to manage and may contain duplicated code.

In our app, we have defined three custom components:

### BatteryWidget.qml

Shows a representation of the battery and its state of charge.

![Application preview](./assets/img/batteryWidget.PNG)

It has a "showOnlyBattery" property that allows the widget to be used on those occasions when only a small icon and colour scale is required (i.e. in the case of the map).

### BikeItemDelegate.qml

Displays an image of the electric bike with price information and a button that opens the detail view.

![Application preview](./assets/img/bikeItemDelegate.PNG)

### BikeMarker.qml

This component generates a thumbnail of the bike together with a charge indicator to be displayed on maps.

![Application preview](./assets/img/bikeMarker.PNG)

## Pages <a name="id6"></a>

### IntroPage.qml

This is the intro page of our application.

![Application preview](./assets/img/intro.PNG)

The main image has an animated overlay that produces a nice immersive effect. This effect is generated from two gradients and animation sequences:

    //Main intro image with animated color overlay
    Rectangle{
        id: backgroundImage
        width: parent.width
        height: parent.height/3*2
        anchors.top: parent.top

        //Background image
        AppImage {
            anchors.fill: parent
            //verticalAlignment: Image.AlignTop
            fillMode: Image.PreserveAspectCrop
            source: "../../assets/img/intro.jpg"
        }

        // Animated overlay
        Rectangle {
            id: overlay
            anchors.fill: parent
            opacity: 0.4

            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    SequentialAnimation on color {
                        loops: Animation.Infinite
                        ColorAnimation { from: "black"; to: "grey"; duration: 3000 }
                        ColorAnimation { from: "grey"; to: "black"; duration: 3000 }
                    }
                }
                GradientStop {
                    position: 1.0
                    SequentialAnimation on color {
                        loops: Animation.Infinite
                        ColorAnimation { from: "yellow"; to: "red"; duration: 3000 }
                        ColorAnimation { from: "red"; to: "yellow"; duration: 3000 }
                    }
                }
            }
        }
    }

The effect of the overlay can be made more intense by varying the level of opacity:

    // Animated overlay
    Rectangle {
        id: overlay
        anchors.fill: parent
        opacity: 0.4

On the other hand, we hide the standard navigation bar with the following code:

    navigationBarHidden: true

The texts and the button are distributed with an element of type *Column*, as shown in the following code:

    // Bottom content
    Column{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: backgroundImage.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: dp(Theme.contentPadding)
        spacing: dp(5)

        //Intro title
        AppText {
            text: "Let's Bike to live healthy"
            font.bold: true
            font.pixelSize: sp(14)
            anchors.horizontalCenter: parent.horizontalCenter
        }

        //Intro description
        AppText {
            text: "With this app you can rent a matching bike or one you want. Let's do it!"
            width: parent.width
            color: Theme.secondaryTextColor
            font.bold: false
            font.pixelSize: sp(8)
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
        }

        //Enter button
        AppButton {
            text: qsTr("Let's Go Bike")
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                console.log("Enter clicked")
                introPage.forceActiveFocus() // move focus away from text fields
                // call login action
                logic.enter()
            }
        }
    }

### HomePage.qml

This is the home page of our application. It is divided into two parts. The upper part contains a title and a search bar with a background image.

The background image contains the same overlay as the image on the Intro page.

In the lower part, the available electric bikes and a counter are displayed.

![Application preview](./assets/img/home.PNG)

Clicking on the search bar will navigate to the search page, emitting the *searchRequested* signal.

                //Searchbar
                AppTextField {
                    width: 240
                    radius: 30
                    height: 35
                    leftPadding: 20
                    color: "#999999"
                    showClearButton: true
                    placeholderText: "Search"
                    font.pixelSize: sp(7)
                    anchors.horizontalCenter: parent.horizontalCenter
                    backgroundColor: "white"

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            homePage.searchRequested("")
                            console.log("Signal fired")
                        }
                    }

                }

To generate the carousel of available electric bikes, we make use of an element of type *AppFlickable* with horizontal scroll and a *Repeater* with a custom delegate *BikeItemDelegate*, defined in the file */qml/components/BikeItemDelegate.qml*.

        //Images and info gallery
        AppFlickable {
            width: parent.width
            height: contentHeight
            flickableDirection: Flickable.HorizontalFlick
            contentWidth: catalogueRow.width
            contentHeight: catalogueRow.height
            Row {
                id: catalogueRow
                spacing:  10
                Repeater {
                    model: dataModel.dataSource
                    BikeItemDelegate {
                        entryModel: modelData
                        onSelected: {
                            homePage.navigationStack.push(detailPageComponent, {"entryModel": modelData})
                        }
                    }
                }
            }
        }

Clicking on the different elements of the carrousel takes you to the detail view of the bicycles, via the push method of the navigationStack of homePage:

          BikeItemDelegate {
              entryModel: modelData
              onSelected: {
                  homePage.navigationStack.push(detailPageComponent, {"entryModel": modelData})
              }
          }

### SearchPage.qml

En esta página el usuario puede buscar y filtrar según la cadena de texto introducida. La barra de búsqueda se localiza en el header y se incluye un botón para limpiar y resetear el campo de texto.

![Application preview](./assets/img/search.PNG)

La personalización de la barra superior de navegación se realiza mediante:

    rightBarItem: NavigationBarItem {
        Row {
            property real backButtonWidth: dp(Theme.navigationBar.height)
            height: dp(Theme.navigationBar.height)
            width: searchPage.width - backButtonWidth
            anchors.right: parent.right

            SearchBar {
                id: searchBar
                inputBackgroundColor: Theme.backgroundColor
                barBackgroundColor: "transparent"
                showClearButton: false
                anchors.verticalCenter: parent.verticalCenter
                width: textField.displayText != "" ? parent.width - clearButton.width - dp(Theme.contentPadding) : parent.width
                Behavior on width {NumberAnimation{duration: 150; easing.type: Easing.InOutQuad}}
                textField.onDisplayTextChanged: dataModel.buildModelUponSearch(textField.displayText)
                iconSize: sp(14)
            }

            AppButton {
                id: clearButton
                flat: true
                text: "Clear"
                anchors.verticalCenter: parent.verticalCenter
                horizontalMargin: 0
                textColor: "white"
                textColorPressed: Qt.darker(Theme.textColor, 1.2)
                textSize: sp(8)

                opacity: searchBar.textField.displayText != ""
                Behavior on opacity {NumberAnimation{duration: 150}}

                onClicked: {
                    searchBar.textField.focus = false
                    searchBar.textField.clear()
                }
            }
        }
    }

On the other hand, an element of type *AppListView* and a delegate of type *BikeItemDelegate* are used to display the results in real time:

    AppListView {
        id: searchResultsList
        anchors.fill: parent
        model: dataModel

        delegate:
            BikeItemDelegate {
            width: parent.width
            entryModel: model
            onSelected: {
                searchPage.navigationStack.push(detailPageComponent, {"entryModel": model})
            }
        }

The event management of the search bar is done in:

    onPushed: {
        if (searchTerm != "") {
            dataModel.buildModelUponSearch(searchTerm)
        } else {
            searchBar.textField.forceActiveFocus()
        }
    }

By clicking on the results you can navigate to the detailed view of the bicycles.

### DetailPage.qml

On this page we can see the details of the selected bikes. A booking button and the main images and features are also displayed.

![Application preview](./assets/img/detail.PNG)

The *entryModel* property stores the bicycle object whose properties will be displayed throughout the page. Initially it is initialised to undefined:

    property var entryModel: undefined

The battery indicator is shown in red and flashes in case the charge is below 25% of its capacity. This element is a custom component available in */qml/components/BatteryWidget.qml*.

      //Battery widget status
      BatteryWidget{
          id: chargeWidget
          batteryCharge: entryModel.currentCharge
          width: dp(80)
          height: dp(35)
          anchors.horizontalCenter: parent.horizontalCenter
      }

The backup button displays an error dialogue if it is pressed. The dialogue is defined by the following code:

    Dialog {
        id: unavailableDialog
        positiveActionLabel: "OK"
        negativeAction: false
        onAccepted: close()
        titleItem.text: "Sorry there was a problem with your request. Please try again later."
        titleItem.fontSize: sp(8)
    }


### Map

This page shows a map with the available electric bikes and their battery level.

![Application preview](./assets/img/map.PNG)

The map is generated using the **AppMap** type, which provides a set of properties, signals and methods to easily work with maps:

    AppMap {
        anchors.fill: parent

        // Default location is Vienna, AT
        center: QtPositioning.coordinate(48.208417, 16.372472)
        zoomLevel: 14

        // Configure map provider
        plugin: Plugin {
            name: system.isPlatform(System.Wasm) ? "mapbox" : "mapboxgl"

            // Set your own map_id and access_token here
            parameters: [
                PluginParameter {
                    name: "mapbox.mapping.map_id"
                    value: "mapbox/streets-v11"
                },
                PluginParameter {
                    name: "mapbox.access_token"
                    value: "pk.eyJ1IjoiZ3R2cGxheSIsImEiOiJjaWZ0Y2pkM2cwMXZqdWVsenJhcGZ3ZDl5In0.6xMVtyc0CkYNYup76iMVNQ"
                },
                PluginParameter {
                    name: "mapbox.mapping.highdpi_tiles"
                    value: true
                }
            ]
        }

        // Map markers for stations
        MapItemView {
           [ ... ]
        }
    }

> In order to use Map elements, you need to include the following line in the *rentbike.pro* file: *QT += location*

The placeholders are defined using *MapItemView* and using a MapQuickItem as delegate. In our case we have used a custom component of type *BikeMarker* that shows the image of the electric bicycle and a battery widget, showing its charge level:

        // Map markers for stations
        MapItemView {
            model: dataModel.dataSource
            delegate: MapQuickItem {
                coordinate: QtPositioning.coordinate(modelData.latitude, modelData.longitude)
                anchorPoint.x: sourceItem.width * 0.5
                anchorPoint.y: sourceItem.height
                sourceItem: BikeMarker{
                    bikeData: modelData
                    MouseArea {
                        anchors.fill: parent
                        onClicked:{
                            console.log("Map icon clicked")
                            mapPage.navigationStack.push(detailPageComponent, {"entryModel": modelData})
                        }

                    }
                }
            }
        }

## Model <a name="id7"></a>

### DataModel.qml

This file contains the data model of the application. In our case, we have used a *JsonListModel* that will store a list of objects in JSON format with all the information about the electric bicycles.

It also includes several auxiliary and filtering functions.

    JsonListModel {
        id: root

        property var dataSource: [
            {
                "name": "Basser",
                "shortDesc": "Best bike in the world",
                "imageLow": "../../assets/bikes/01_200x135",
                "imageHigh": "../../assets/bikes/01_900x608",
                "price": "35",
                "currentCharge": 1.0,
                "latitude": 48.210425,
                "longitude": 16.3561,
                "longDesc": "<ul>
                            <li><b>Acceleration</b>: Gain extra momentum
                            </ul>
                            [...]"
            },
            {
                "name": "Mounller",
                "shortDesc": "Best bike in the world",
                "imageLow": "../../assets/bikes/02_200x135",
                "imageHigh": "../../assets/bikes/02_900x600",
                "price": "35",
                "currentCharge": 0.16,
                "latitude": 48.210666083418,
                "longitude": 16.37298586854365,
                "longDesc": "<ul>
                            <li><b>Acceleration</b>: Gain extra momentum
                            </ul>
                            [...]"
            }
            [...]
        ]

        source: dataSource

        // hasReference is used to indicate if term can be found amoung values in JS object (entry)
        function hasReference(entry, term) {
            [...]
        }

        // buildModelUponSearch cleans model and adds these entries which have references to search request
        function buildModelUponSearch(term="") {
            [...]
        }

        function getImageLow(entry) {
            [...]
        }

        function getImageHigh(entry) {
            [...]
        }
    }


### AppModel.qml

It manages the data model of the application, connected to the Logic element. In our case, it only stores whether the user has entered the application through the *userEntered* variable.

    Item {
        id: appModel

        // property to configure target dispatcher / logic
        property alias dispatcher: logicConnection.target

        // whether a user has entered the app
        readonly property bool userEntered: _.userEntered

        // listen to actions from dispatcher
        Connections {
            id: logicConnection

            //enter
            onEnter: _.userEntered = true

            //exit
            onExit: _.userEntered = false
        }

        // private
        Item {
            id: _

            // basic auth
            property bool userEntered: false
        }
    }

## Logic <a name="id8"></a>

In our app, this *Logic.qml* file only contains the signals that are used for the input and output events of the application:

      Item {

          signal enter()
          signal exit()

      }

## App publication <a name="id10"></a>

To publish your application on the platform of your choice, we recommend that you follow this [Felgo's great tutorial](https://felgo.com/doc/felgo-publishing/) which explains in detail how to build and publish your app.

![Application preview](./assets/mockups/mockup04.png)
