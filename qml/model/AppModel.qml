import QtQuick 2.0
import Felgo 3.0

Item {
    id: appModel

    // property to configure target dispatcher / logic
    property alias dispatcher: logicConnection.target

    // whether a user has entered the app
    readonly property bool userEntered: _.userEntered

    // listen to actions from dispatcher
    Connections {
        id: logicConnection

        //enter
        onEnter: _.userEntered = true

        //exit
        onExit: _.userEntered = false
    }

    // private
    Item {
        id: _

        // basic auth
        property bool userEntered: false
    }
}



