import QtQuick 2.0
import Felgo 3.0

JsonListModel {
    id: root

    property var dataSource: [
        {
            "name": "Basser",
            "shortDesc": "Best bike in the world",
            "imageLow": "../../assets/bikes/01_200x135",
            "imageHigh": "../../assets/bikes/01_900x608",
            "price": "35",
            "currentCharge": 1.0,
            "latitude": 48.210425,
            "longitude": 16.3561,
            "longDesc": "<ul>
                        <li><b>Acceleration</b>: Gain extra momentum with power right behind you the faster you pedal.</li>
                        <li><b>Flow</b>: Ride smooth and steady with a predictable source of power.</li>
                        <li><b>Incline</b>: Conquer the hills effortlessly for a new vantage point on your daily routine.</li>
                        <li><b>Theft Detection</b>: Be notified with Theft Alerts* if someone tries to snatch your bike. GPS tracking will help you get it back.</b></li>
                        </ul>"
        },
        {
            "name": "Mounller",
            "shortDesc": "Best bike in the world",
            "imageLow": "../../assets/bikes/02_200x135",
            "imageHigh": "../../assets/bikes/02_900x600",
            "price": "35",
            "currentCharge": 0.16,
            "latitude": 48.210666083418,
            "longitude": 16.37298586854365,
            "longDesc": "<ul>
                        <li><b>Acceleration</b>: Gain extra momentum with power right behind you the faster you pedal.</li>
                        <li><b>Flow</b>: Ride smooth and steady with a predictable source of power.</li>
                        <li><b>Incline</b>: Conquer the hills effortlessly for a new vantage point on your daily routine.</li>
                        <li><b>Theft Detection</b>: Be notified with Theft Alerts* if someone tries to snatch your bike. GPS tracking will help you get it back.</b></li>
                        </ul>"
        },
        {
            "name": "Planer",
            "shortDesc": "Best bike in the world",
            "imageLow": "../../assets/bikes/03_200x133",
            "imageHigh": "../../assets/bikes/03_900x600",
            "price": "45",
            "currentCharge": 0.56,
            "latitude": 48.203366,
            "longitude": 16.376719,
            "longDesc": "<ul>
                        <li><b>Acceleration</b>: Gain extra momentum with power right behind you the faster you pedal.</li>
                        <li><b>Flow</b>: Ride smooth and steady with a predictable source of power.</li>
                        <li><b>Incline</b>: Conquer the hills effortlessly for a new vantage point on your daily routine.</li>
                        <li><b>Theft Detection</b>: Be notified with Theft Alerts* if someone tries to snatch your bike. GPS tracking will help you get it back.</b></li>
                        </ul>"
        },
        {
            "name": "Oxama",
            "shortDesc": "Best bike in the world",
            "imageLow": "../../assets/bikes/04_200x135",
            "imageHigh": "../../assets/bikes/04_900x600",
            "price": "55",
            "currentCharge": 0.79,
            "latitude": 48.211534,
            "longitude": 16.382375,
            "longDesc": "<ul>
                        <li><b>Acceleration</b>: Gain extra momentum with power right behind you the faster you pedal.</li>
                        <li><b>Flow</b>: Ride smooth and steady with a predictable source of power.</li>
                        <li><b>Incline</b>: Conquer the hills effortlessly for a new vantage point on your daily routine.</li>
                        <li><b>Theft Detection</b>: Be notified with Theft Alerts* if someone tries to snatch your bike. GPS tracking will help you get it back.</b></li>
                        </ul>"
        },
        {
            "name": "Oxama",
            "shortDesc": "Best bike in the world",
            "imageLow": "../../assets/bikes/04_200x135",
            "imageHigh": "../../assets/bikes/04_900x600",
            "price": "55",
            "currentCharge": 0,
            "latitude": 48.214534,
            "longitude": 16.352375,
            "longDesc": "<ul>
                        <li><b>Acceleration</b>: Gain extra momentum with power right behind you the faster you pedal.</li>
                        <li><b>Flow</b>: Ride smooth and steady with a predictable source of power.</li>
                        <li><b>Incline</b>: Conquer the hills effortlessly for a new vantage point on your daily routine.</li>
                        <li><b>Theft Detection</b>: Be notified with Theft Alerts* if someone tries to snatch your bike. GPS tracking will help you get it back.</b></li>
                        </ul>"
        }
    ]

    source: dataSource

    // hasReference is used to indicate if term can be found amoung values in JS object (entry)
    function hasReference(entry, term) {

        if (!entry || entry === undefined) {
            return false
        }

        for (const field in entry) {

            var type = typeof entry[field]
            if (type === "string") {

                var value = entry[field].toLowerCase()
                if (value.startsWith(term.toLowerCase())) {
                    return true
                }
            } else if (type === "object") {

                for (const value of entry[field]) {
                    if (value.toLowerCase().startsWith(term.toLowerCase())) {
                        return true
                    }
                }
            }
        }
        return false
    }

    // buildModelUponSearch cleans model and adds these entries which have references to search request
    function buildModelUponSearch(term="") {
        root.remove(0, root.count)

        for (const entry of root.dataSource) {

            if (hasReference(entry, term)) {
                root.append(entry)
            }
        }
    }

    function getImageLow(entry) {
        if (entry === undefined) {
            console.error("Undefined image")
            return "" //TODO: some placeholder here
        }
        return Qt.resolvedUrl(entry.imageLow)
    }

    function getImageHigh(entry) {
        if (entry === undefined) {
            console.error("Undefined image")
            return "" //TODO: some placeholder here
        }
        return Qt.resolvedUrl(entry.imageHigh)
    }
}



