import Felgo 3.0
import QtQuick 2.0
import "model"
import "logic"
import "pages"
import "components"

App {
    // You get free licenseKeys from https://felgo.com/licenseKey
    // With a licenseKey you can:
    //  * Publish your games & apps for the app stores
    //  * Remove the Felgo Splash Screen or set a custom one (available with the Pro Licenses)
    //  * Add plugins to monetize, analyze & improve your apps (available with the Pro Licenses)
    //licenseKey: "<generate one from https://felgo.com/licenseKey>"

    // initialize theme styles
    onInitTheme: {
        //fonts
        Theme.normalFont = poppinsFontNormal
        Theme.boldFont = poppinsFontBold

        //buttons
        Theme.appButton.backgroundColor = 'black'
        Theme.appButton.radius = sp(30)
        Theme.appButton.flatTextColor = 'white'
        Theme.appButton.dropShadow = false
        Theme.appButton.textSize = sp(4)
        Theme.appButton.textColor = 'white'
        Theme.appButton.textColorDisabled = 'white'

        //colors
        Theme.colors.textColor = 'black'
        Theme.colors.secondaryTextColor = 'grey'
        Theme.colors.secondaryBackgroundColor = 'black'
        Theme.colors.tintColor = 'grey'

        //navigation bar
        //Theme.navigationBar.backgroundImageSource= "../assets/img/background.jpg"
        //Theme.navigationBar.backgroundImageFillMode= Image.PreserveAspectCrop
        Theme.navigationBar.backgroundColor= 'black'
        Theme.navigationBar.shadowHeight= 0
        Theme.navigationBar.itemColor= 'white'

        //dialogs
        Theme.dialog.titleTextSize = 12
        Theme.dialog.buttonTextSize = 12
    }

    // load new fonts with FontLoader
    FontLoader {
        id: poppinsFontNormal
        source: "../assets/fonts/Poppins-Regular.ttf" // loaded from your assets folder
    }

    FontLoader {
        id: poppinsFontBold
        source: "../assets/fonts/Poppins-SemiBold.ttf" // loaded from your assets folder
    }

    // business logic
    Logic {
        id: logic
    }

    // data model for storing bikes
    DataModel {
        id: dataModel
    }

    // app model for store user connection stares
    AppModel {
        id: appModel
        dispatcher: logic //handles actions sent by logic
    }

    // view
    Navigation {
        id: navigation

        // only enable if user has entered in
        // intro page below overlays navigation then
        enabled: appModel.userEntered

        // Image at top
        headerView: AppImage {
            width: parent.width
            height: 100
            source: "../assets/img/bikeBackground.png"
            fillMode: AppImage.PreserveAspectCrop
        }

        // Exit button at footer
        footerView: AppButton {
            text: "EXIT"
            flat: true
            textColor: 'black'
            textSize: sp(15)
            onClicked: {
                logic.exit()
            }
        }

        // Home tab
        NavigationItem {
            title: qsTr("Home")
            icon: IconType.home
            NavigationStack {
                id: homeNavigationStack
                splitView: tablet // use side-by-side view on tablets
                initialPage: HomePage {
                    onSearchRequested: homeNavigationStack.push(searchPageComponent, {"searchTerm": term})
                }
            }
        }

        // Search tab
        NavigationItem {
            title: qsTr("Search")
            icon: IconType.search
            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: SearchPage { }
            }
        }

        // Map tab
        NavigationItem {
            title: qsTr("Map")
            icon: IconType.mapmarker
            NavigationStack {
                splitView: tablet // use side-by-side view on tablets
                initialPage: MapPage { }
            }
        }
    }

    Component {
        id: detailPageComponent
        DetailPage { }
    }

    Component {
        id: searchPageComponent
        SearchPage { }
    }

    // Intro page lies on top of previous items and overlays if user is not entered in
    IntroPage {
        visible: opacity > 0
        enabled: visible
        opacity: appModel.userEntered ? 0 : 1 // hide if user is entered in
        Behavior on opacity { NumberAnimation { duration: 250 } } // page fade in/out
    }
}
