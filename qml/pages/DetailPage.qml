import QtQuick 2.0
import QtQuick.Controls 2.0
import Felgo 3.0
import QtQuick.Layouts 1.0
import "../components"

Page {

    id: detailPage

    property var entryModel: undefined

    backNavigationEnabled: true

    //Custom navigation header
    rightBarItem: NavigationBarItem {
        Row {
            property real backButtonWidth: dp(Theme.navigationBar.height)
            height: dp(Theme.navigationBar.height)
            anchors.right: parent.right
            anchors.rightMargin: dp(Theme.contentPadding)

            AppButton {
                id: clearButton
                borderWidth: 2
                borderColor: "white"
                text: "Book Now"
                anchors.verticalCenter: parent.verticalCenter
                horizontalMargin: 0
                textColor: "white"
                textSize: sp(8)
                onClicked: unavailableDialog.open() //TODO: implement booking logic
            }
        }
    }

    //Images and info gallery
    AppFlickable {
        anchors.fill: parent
        contentHeight: detailsContent.height

        Column{
            id: detailsContent
            width: parent.width

            //Main bike image
            AppImage {
                id: productImage
                width: parent.width*0.9
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.PreserveAspectFit
                source: Qt.resolvedUrl(dataModel.getImageHigh(entryModel))
                antialiasing: true
                smooth: true
            }

            //Name
            AppText {
                text: entryModel.name
                font.bold: true
                font.pixelSize: sp(19)
                anchors.horizontalCenter: parent.horizontalCenter
            }

            //Price
            AppText {
                text: entryModel.price + '$/hour'
                color: Theme.colors.secondaryTextColor
                font.bold: false
                font.pixelSize: sp(10)
                anchors.horizontalCenter: parent.horizontalCenter
            }

            //Intro description
            AppText {
                id: descTitle
                text: entryModel.shortDesc
                font.pixelSize: sp(10)
                anchors.horizontalCenter: parent.horizontalCenter
                bottomPadding: dp(Theme.contentPadding)
            }

            //Battery widget status
            BatteryWidget{
                id: chargeWidget
                batteryCharge: entryModel.currentCharge
                width: dp(80)
                height: dp(35)
                anchors.horizontalCenter: parent.horizontalCenter
            }

            //Description
            AppText {
                topPadding: dp(Theme.contentPadding)
                bottomPadding: dp(Theme.contentPadding)

                width: parent.width * 0.9
                font.pixelSize: sp(6)
                wrapMode: Text.WordWrap
                textFormat: AppText.RichText
                text: entryModel.longDesc
            }
        }

    }

    Dialog {
        id: unavailableDialog
        positiveActionLabel: "OK"
        negativeAction: false
        onAccepted: close()
        titleItem.text: "Sorry there was a problem with your request. Please try again later."
        titleItem.fontSize: sp(8)
    }
}
