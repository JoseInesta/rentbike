import Felgo 3.0
import QtQuick 2.0
import QtGraphicalEffects 1.0
import "../components"

Page {

    id: searchPage

    property alias searchTerm: searchBar.text

    onPushed: {
        if (searchTerm != "") {
            dataModel.buildModelUponSearch(searchTerm)
        } else {
            searchBar.textField.forceActiveFocus()
        }
    }

    onDisappeared: {
        dataModel.populate() //restore the model
    }

    rightBarItem: NavigationBarItem {
        Row {
            property real backButtonWidth: dp(Theme.navigationBar.height)
            height: dp(Theme.navigationBar.height)
            width: searchPage.width - backButtonWidth
            anchors.right: parent.right

            SearchBar {
                id: searchBar
                inputBackgroundColor: Theme.backgroundColor
                barBackgroundColor: "transparent"
                showClearButton: false
                anchors.verticalCenter: parent.verticalCenter
                width: textField.displayText != "" ? parent.width - clearButton.width - dp(Theme.contentPadding) : parent.width
                Behavior on width {NumberAnimation{duration: 150; easing.type: Easing.InOutQuad}}
                textField.onDisplayTextChanged: dataModel.buildModelUponSearch(textField.displayText)
                iconSize: sp(14)
            }

            AppButton {
                id: clearButton
                flat: true
                text: "Clear"
                anchors.verticalCenter: parent.verticalCenter
                horizontalMargin: 0
                textColor: "white"
                textColorPressed: Qt.darker(Theme.textColor, 1.2)
                textSize: sp(8)

                opacity: searchBar.textField.displayText != ""
                Behavior on opacity {NumberAnimation{duration: 150}}

                onClicked: {
                    searchBar.textField.focus = false
                    searchBar.textField.clear()
                }
            }
        }
    }

    AppListView {
        id: searchResultsList
        anchors.fill: parent
        model: dataModel

        delegate:
            BikeItemDelegate {
            width: parent.width
            entryModel: model
            onSelected: {
                searchPage.navigationStack.push(detailPageComponent, {"entryModel": model})
            }
        }

        emptyView.children: [
            Column {
                anchors.centerIn: parent
                width: searchResultsList.width * 0.75
                spacing: dp(10)

                AppText {
                    width: parent.width
                    text: qsTr("No bikes found")
                    fontSize: sp(10)
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                }

                AppText {
                    width: parent.width
                    color: Theme.secondaryTextColor
                    fontSize: sp(6)
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Search for your favourite bike")
                    wrapMode: Text.WordWrap
                }
            }
        ]
    }
}
