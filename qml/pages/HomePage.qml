import QtQuick 2.9
import Felgo 3.0
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import "../components"

FlickablePage {

    id: homePage

    navigationBarHidden: true
    signal searchRequested(string term)

    // header content
    Column {
        id: content
        width: parent.width

        Rectangle {
            height: dp(200)
            width: parent.width
            color: "transparent"

            AppImage {
                id: bannerImage
                width: parent.width
                anchors.bottom: parent.bottom
                height: homePage.flickable.contentY < 0 ? parent.height - homePage.flickable.contentY : parent.height
                //fillMode: AppImage.PreserveAspectFit
                fillMode: AppImage.PreserveAspectCrop
                //source: "../../assets/img/bikeBackground.png"
                source: "../../assets/img/background.jpg"
                opacity: 1

                // Animated overlay
                Rectangle {
                    id: overlay
                    anchors.fill: parent
                    opacity: 0.4
                    gradient:
                        Gradient {
                        GradientStop {
                            position: 0.0
                            SequentialAnimation on color {
                                loops: Animation.Infinite
                                ColorAnimation { from: "black"; to: "grey"; duration: 3000 }
                                ColorAnimation { from: "grey"; to: "black"; duration: 3000 }
                            }
                        }
                        GradientStop {
                            position: 1.0
                            SequentialAnimation on color {
                                loops: Animation.Infinite
                                ColorAnimation { from: "yellow"; to: "red"; duration: 3000 }
                                ColorAnimation { from: "red"; to: "yellow"; duration: 3000 }
                            }
                        }
                    }
                }
            }

            Column {
                width: parent.width
                anchors.top: parent.top
                anchors.topMargin: dp(50)
                //spacing: dp(Theme.navigationBar.defaultBarItemPadding)
                spacing: dp(3)
                //Intro title
                AppText {
                    text: "Let's Bike"
                    width: parent.width
                    color: "white"
                    font.bold: true
                    font.pixelSize: sp(14)
                    horizontalAlignment: AppText.AlignHCenter
                }

                //Intro description
                AppText {
                    text: "Select bike you want"
                    width: parent.width
                    color: "white"
                    font.bold: false
                    font.pixelSize: sp(9)
                    horizontalAlignment: AppText.AlignHCenter
                }

                //Searchbar
                AppTextField {
                    width: 240
                    radius: 30
                    height: 35
                    leftPadding: 20
                    color: "#999999"
                    showClearButton: true
                    placeholderText: "Search"
                    font.pixelSize: sp(7)
                    anchors.horizontalCenter: parent.horizontalCenter
                    backgroundColor: "white"

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            homePage.searchRequested("")
                            console.log("Signal fired")
                        }
                    }

                }
            }
        }
    }

    //Horizontal Bikes Gallery
    Column {
        anchors.top: content.bottom
        anchors.topMargin: dp(Theme.contentPadding)
        width: parent.width

        //Title and link to all
        Row{
            leftPadding: dp(Theme.contentPadding)
            rightPadding: dp(Theme.contentPadding)

            width: parent.width - 2*dp(Theme.contentPadding)
            AppText {
                text: "Our bikes"
                color: Theme.primaryTextColor
                width: parent.width/2
                font.bold: true
                font.pixelSize: sp(12)
                horizontalAlignment: Text.AlignLeft
            }

            AppText {
                text: dataModel.dataSource.length + ' availables'
                color: Theme.secondaryTextColor
                width: parent.width/2
                opacity: 1
                font.pixelSize: sp(8)
                horizontalAlignment: Text.AlignRight
            }
        }

        //Images and info gallery
        AppFlickable {
            width: parent.width
            height: contentHeight
            flickableDirection: Flickable.HorizontalFlick
            contentWidth: catalogueRow.width
            contentHeight: catalogueRow.height
            Row {
                id: catalogueRow
                spacing:  10
                Repeater {
                    model: dataModel.dataSource
                    BikeItemDelegate {
                        entryModel: modelData
                        onSelected: {
                            homePage.navigationStack.push(detailPageComponent, {"entryModel": modelData})
                        }
                    }
                }
            }
        }
    }
}

