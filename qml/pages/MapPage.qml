import Felgo 3.0
import QtQuick 2.12
import QtLocation 5.12
import QtPositioning 5.12
import "../components"

Page {

    id: mapPage

    AppMap {
        anchors.fill: parent

        // Default location is Vienna, AT
        center: QtPositioning.coordinate(48.208417, 16.372472)
        zoomLevel: 14

        // Configure map provider
        plugin: Plugin {
            name: system.isPlatform(System.Wasm) ? "mapbox" : "mapboxgl"

            // Set your own map_id and access_token here
            parameters: [
                PluginParameter {
                    name: "mapbox.mapping.map_id"
                    value: "mapbox/streets-v11"
                },
                PluginParameter {
                    name: "mapbox.access_token"
                    value: "pk.eyJ1IjoiZ3R2cGxheSIsImEiOiJjaWZ0Y2pkM2cwMXZqdWVsenJhcGZ3ZDl5In0.6xMVtyc0CkYNYup76iMVNQ"
                },
                PluginParameter {
                    name: "mapbox.mapping.highdpi_tiles"
                    value: true
                }
            ]
        }

        // Map markers for stations
        MapItemView {
            model: dataModel.dataSource
            delegate: MapQuickItem {
                coordinate: QtPositioning.coordinate(modelData.latitude, modelData.longitude)
                anchorPoint.x: sourceItem.width * 0.5
                anchorPoint.y: sourceItem.height
                sourceItem: BikeMarker{
                    bikeData: modelData
                    MouseArea {
                        anchors.fill: parent
                        onClicked:{
                            console.log("Map icon clicked")
                            mapPage.navigationStack.push(detailPageComponent, {"entryModel": modelData})
                        }
                    }
                }
            }
        }
    }
}
