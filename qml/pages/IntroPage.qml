import QtQuick 2.0
import Felgo 3.0
import QtQuick.Layouts 1.1

Page {

    id: introPage

    navigationBarHidden: true

    //Main intro image with animated color overlay
    Rectangle{
        id: backgroundImage
        width: parent.width
        height: parent.height/3*2
        anchors.top: parent.top

        //Background image
        AppImage {
            anchors.fill: parent
            //verticalAlignment: Image.AlignTop
            fillMode: Image.PreserveAspectCrop
            source: "../../assets/img/intro.jpg"
        }

        // Animated overlay
        Rectangle {
            id: overlay
            anchors.fill: parent
            opacity: 0.4

            gradient: Gradient {
                GradientStop {
                    position: 0.0
                    SequentialAnimation on color {
                        loops: Animation.Infinite
                        ColorAnimation { from: "black"; to: "grey"; duration: 3000 }
                        ColorAnimation { from: "grey"; to: "black"; duration: 3000 }
                    }
                }
                GradientStop {
                    position: 1.0
                    SequentialAnimation on color {
                        loops: Animation.Infinite
                        ColorAnimation { from: "yellow"; to: "red"; duration: 3000 }
                        ColorAnimation { from: "red"; to: "yellow"; duration: 3000 }
                    }
                }
            }
        }
    }

    // Bottom content
    Column{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.top: backgroundImage.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: dp(Theme.contentPadding)
        spacing: dp(5)

        //Intro title
        AppText {
            text: "Let's Bike to live healthy"
            font.bold: true
            font.pixelSize: sp(14)
            anchors.horizontalCenter: parent.horizontalCenter
        }

        //Intro description
        AppText {
            text: "With this app you can rent a matching bike or one you want. Let's do it!"
            width: parent.width
            color: Theme.secondaryTextColor
            font.bold: false
            font.pixelSize: sp(8)
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
        }

        //Enter button
        AppButton {
            text: qsTr("Let's Go Bike")
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                console.log("Enter clicked")
                introPage.forceActiveFocus() // move focus away from text fields
                // emit signal enter action
                logic.enter()
            }
        }
    }
}
