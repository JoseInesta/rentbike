import Felgo 3.0
import QtQuick 2.0

Item {
    id: root

    property var entryModel

    signal selected()

    width: dp(120)
    height: dp(130)

    AppImage {
        id: bikeMainImage
        width: parent.width
        source: Qt.resolvedUrl(dataModel.getImageLow(entryModel))
        fillMode: AppImage.PreserveAspectFit
    }

    AppButton {
        text: entryModel.name + ' ' + entryModel.price + '$/hour'
        anchors.top: bikeMainImage.bottom
        anchors.topMargin: dp(0)
        anchors.horizontalCenter: parent.horizontalCenter
        borderColor: "black"
        backgroundColor: "white"
        textColor: "black"
        dropShadow: false
        borderWidth: 1
        textSize: sp(8)
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.selected()
    }
}
