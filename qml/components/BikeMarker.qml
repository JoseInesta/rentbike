import Felgo 3.0
import QtQuick 2.0

Rectangle{

    property var bikeData: undefined

    radius: dp(10)
    width: dp(50)
    height: dp(50)
    color: "transparent"

    AppImage {
        id: bikeImage
        width: parent.width
        anchors.margins: 15
        fillMode: Image.PreserveAspectFit
        source: Qt.resolvedUrl(dataModel.getImageLow(bikeData))
    }

    BatteryWidget{
        anchors.top: bikeImage.bottom
        showOnlyBattery: true
        batteryCharge: bikeData.currentCharge
        width: 30
        height: 15
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
