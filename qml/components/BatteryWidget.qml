import Felgo 3.0
import QtQuick 2.0
import QtQuick.Layouts 1.0

Rectangle{

    property real batteryCharge: 0.0
    property bool showOnlyBattery: false

    readonly property int lowBatteryBlinkInterval : 1000 //ms

    function getColor(){
        if(0 < batteryCharge < 0.25) return "red"
        if(0.25 < batteryCharge < 0.5) return "orange"
        if(batteryCharge > 0.5) return "green"
    }

    id: chargeWidget
    border.color: getColor()
    border.width: showOnlyBattery ? 0 : 2 //Hide border if showOnlyBattery is true
    radius: showOnlyBattery ? 0 : 20 //Hide radius if showOnlyBattery is true

    color: !showOnlyBattery ? "transparent" : getColor()

    //If battery low make border color blink from black to red
    SequentialAnimation on border.color {
        running: batteryCharge < 0.25
        loops: Animation.Infinite
        ColorAnimation { from: "black"; to: "red"; duration: lowBatteryBlinkInterval }
        ColorAnimation { from: "red"; to: "black"; duration: lowBatteryBlinkInterval }
    }

    //Mini icon is visible when showOnlyBattery is true
    //Used for visualization in map
    AppImage {
        visible: showOnlyBattery
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        source: "../../assets/icons/battery.png"
        antialiasing: true
        smooth: true
    }

    //Normal widget only visible if showOnlyBattery is false
    //Used for visualization in detail page
    RowLayout {
        visible: !showOnlyBattery
        id: layout
        anchors.fill: parent
        spacing: 0

        //Battery icon
        Rectangle {
            color: 'transparent'
            Layout.fillWidth: true
            Layout.minimumWidth: parent.width/2
            Layout.preferredWidth: dp(150)
            Layout.maximumWidth: parent.width/2
            Layout.minimumHeight: parent.height
            AppImage {
                anchors.centerIn: parent
                width: parent.width - 20
                fillMode: Image.PreserveAspectFit
                source: "../../assets/icons/battery.png"
                antialiasing: true
                smooth: true
            }
        }

        //Battery title
        Rectangle {
            color: 'transparent'
            Layout.fillWidth: true
            Layout.minimumWidth: parent.width/2
            Layout.preferredWidth: parent.width/2
            Layout.preferredHeight: parent.height
            AppText{
                text: {
                    let charge = batteryCharge * 100
                    charge.toFixed(0) + '%' //avoid multiple decimals
                }
                // color: getColor()
                lineHeight: 1
                fontSize: sp(6)
                font.bold: true
                anchors.fill: parent
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                //If battery low make font color blink from black to red
                SequentialAnimation on color {
                    running: batteryCharge < 0.25
                    loops: Animation.Infinite
                    ColorAnimation { from: "black"; to: "red"; duration: lowBatteryBlinkInterval }
                    ColorAnimation { from: "red"; to: "black"; duration: lowBatteryBlinkInterval }
                }
            }
        }
    }
}
